
# Example Project for DotNetStandard

This project was created using Visual Studio Community 2017
Includes base dependency `NetStandardLibrary`, `NUnit.Console`

It's information from NuGet:

```
https://api-v2v3search-0.nuget.org/query?q=NetStandardLibrary&prerelease=false

# open this
https://api.nuget.org/v3/registration3/nunit.console/3.7.0.json
# find this and open and you get dependencies
https://api.nuget.org/v3/catalog0/data/2017.07.14.00.54.16/nunit.console.3.7.0.json
```

## Notes

* The default `.gitignore` for VSCode2017 ignores the `obj` folder which contains the `project.assets` file which contains a detailed list of dependencies.